import { View, Text } from 'react-native'
import React from 'react'
import Home from '../Home'
import Messages from '../Messages'
import Settings from '../Setting'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const BottomTabs = () => {
    const Tab = createMaterialBottomTabNavigator();
    return (
        <Tab.Navigator
          initialRouteName="tabs_home"
          screenOptions={{
            tabBarActiveTintColor: '#11929E',
            headerShown:true
          }}
        >
          <Tab.Screen
            name="tabs_home"
            component={Home}
            options={{
              tabBarLabel: 'Dashboard',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen
            name="messages"
            component={Messages}
            options={{
              tabBarLabel: 'Messages',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="chat" color={color} size={size} />
              ),
              //tabBarBadge: 1,
            }}
          />
          <Tab.Screen
            name="settings"
            component={Settings}
            options={{
              tabBarLabel: 'Paramètre',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="account-settings-outline" color={color} size={size} />
              ),
            }}
          />
        </Tab.Navigator>
      );
}

export default BottomTabs