import {StyleSheet} from 'react-native'
import {PADDING} from '../../outils/constantes'

const dashboardStyles=StyleSheet.create({
    header:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
        backgroundColor:'white'

    },
    userImg:{
        width:70,
        height:70,
        borderRadius:70/2,
    },
    userName:{
       fontSize:16
    },

    //Styliser la flatlist

    scrollableList:{
        paddingHorizontal:PADDING.horizontal,
        paddingVertical:PADDING.vertical
    },
    scrollableListItem:{
        flexDirection:'column',
            paddingHorizontal:15,
            paddingVertical:15,
            marginRight:15,
            backgroundColor:'white',
            elevation:1
    },
   mainText:{
    fontWeight:'bold',
    fontSize:16

   },
   subText:{
    marginTop:10,
    fontSize:12
   }
    })



    export default dashboardStyles;