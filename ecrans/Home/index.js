import { View, Text, ScrollView,Image ,FlatList,TouchableOpacity} from 'react-native'
import React from 'react'
import dashboardStyles from'./style.js'
import { FakeActivity } from '../../fakeData/fakeActivity.js'
import SVG_HOSPITAL from '../../assets/images/svg/undraw_medicine_b-1-ol.svg'

const Home = () => {
  return (
   <ScrollView>
    {/* Debut du header */}
    <View style={dashboardStyles.header}> 
      <Text style={dashboardStyles.userName}>Mansa Musa</Text>
      <Image style={dashboardStyles.userImg} source={require('./../../assets/mansa-musa.jpg')}/>
    </View>
    {/* fin du header */}


 {/* Liste des activités*/}

 <FlatList
  data={FakeActivity} 
  keyExtractor={item=>item.id} 
  horizontal={true}
  showsHorizontalScrollIndicator={false}
  style={dashboardStyles.scrollableList}
  renderItem={({item})=>{
    return(
      <TouchableOpacity style={dashboardStyles.scrollableListItem}>
        {/* <SVG_HOSPITAL width={48} height={48}/> */}
        <Text style={dashboardStyles.mainText}>{item.mainText}</Text>
        <Text style={dashboardStyles.subText}>{item.subText}</Text>
      </TouchableOpacity>
    )
  }}
  />
 {/* Fin Liste des activités*/}
   </ScrollView>
  )
}

export default Home